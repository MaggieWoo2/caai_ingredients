import pandas as pd
import numpy as np
import regex as re

data = pd.read_excel('/Users/maggiewu/Desktop/CAAI/Schema-3.0.xlsx', sheet_name='Ingredients')
data = data.loc[:, 'Tag/item name':'Notes']
# print(data.head())

pd.set_option('display.width', 320)
pd.set_option('display.max_columns', 10)


def clean_raw_ingredients(df):
    """delete blank rows, remove NAs, convert CSV into lists...
    return a clean dataset"""

    i = df[df['Tag/item name'].isna()].index
    df = df.drop(data.index[i], axis=0)
    df.reset_index()
    j = df[df.isnull().sum(axis=1) > 4].index
    df = df.drop(data.index[j], axis=0)
    df.reset_index()
    # remove NAs, convert into list
    for col in df.columns[1:5]:
        df[col] = df[col].fillna('')
        df[col] = df[col].str.split(', ')
        # df[col] = df[col].str.split('; ')
    # Attribute defaults
    # df['Attribute defaults'] = df['Attribute defaults'].str.replace('%', 'percentage')

    # turn the first 2 cols into list
    df.to_string()
    return df


data = clean_raw_ingredients(data)


def get_ingredient_info_base(ingredients_dataset, ingredient):
    """a function to check the data for a given ingredient, without inheriting from parents
    return inherit_from, attribute_default"""
    for idx, item in enumerate(ingredients_dataset['Tag/item name']):
        if ingredient == item:
            return ingredients_dataset.iloc[idx, 1], ingredients_dataset.iloc[idx, 3]


def get_ingredient_info_all(ingredients_dataset, ingredient):
    """with inheriting from parents, use get_ingredient_info_base()"""
    base_i_f, base_a_d = get_ingredient_info_base(ingredients_dataset, ingredient)
    temp_i_f, temp_a_d = [], []
    if len(base_i_f) > 0:
        for i in base_i_f:
            if i in list(ingredients_dataset['Tag/item name']):
                parent_i_f, parent_a_d = get_ingredient_info_base(ingredients_dataset, i)
                temp_i_f += parent_i_f
                temp_a_d += parent_a_d
    #             else:
    #                 temp_i_f += i
    #             temp_a_d += parent_a_d
    i_f = list(set(base_i_f + temp_i_f))
    a_d = list(set(base_a_d + temp_a_d))
    if '' in i_f: i_f.remove('')
    if '' in a_d: a_d.remove('')
    return (i_f, a_d)


# print(get_ingredient_info_all(data,'Salted Butter'))
data['Inherits from'] = [get_ingredient_info_all(data, i)[0] for i in data['Tag/item name']]
data['Attribute defaults'] = [get_ingredient_info_all(data, i)[1] for i in data['Tag/item name']]
print(data.head(50))

