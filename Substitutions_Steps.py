import pandas as pd
import numpy as np
import regex as re
pd.set_option('display.max_columns', 10)

substitutions = pd.read_excel('/Users/maggiewu/Desktop/CAAI/Schema-3.0.xlsx', sheet_name='new Substitutions')
steps = pd.read_excel('/Users/maggiewu/Desktop/CAAI/Schema-3.0.xlsx', sheet_name='new Steps')
substitutions = substitutions.loc[:, :'Explanation']
steps = steps.loc[:, :'Explanation']

substitutions.insert(0, 'id', [i for i in range(len(substitutions))])
substitutions.set_index('id')
steps.insert(0, 'id', ['Step_'+str(i) for i in range(len(steps))])
steps.set_index('id')


substitutions = substitutions.replace('X', np.nan)
steps = steps.replace('X', np.nan)

steps['Required items'] = steps['Required items'].str.split(',')
# print(steps.head())

# Data cleaning notes: 1. change time and complexity to number only
# 2. in items out/in column, do not include and. if there is 'or' relationship, do not include , (eg. a, b, c. a or b or c)

steps['Complexity'] = steps['Complexity'].replace('Depends', 0) #maybe modified later (change Depends to 0 for calculation)
steps['Complexity'] = steps['Complexity'].replace(np.nan, 0)

substitutions['Items in'] = substitutions['Items in'].str.split(',')
substitutions['Items out'] = substitutions['Items out'].str.split(',')
substitutions['Items out'] = substitutions['Items out'].replace(np.nan, None)
# substitutions['Scope'] = substitutions['Scope'].str.split(',')
substitutions['Scope'] = substitutions['Scope'].replace(np.nan, None)
# print(substitutions.head())

# with pd.ExcelWriter('/Users/maggiewu/Desktop/CAAI/temp_substitution.xlsx') as writer:
#     substitutions.to_excel(writer, sheet_name='substitution')
#     steps.to_excel(writer, sheet_name='steps')

# substitutions = pd.read_excel('/Users/maggiewu/Desktop/CAAI/temp_substitution.xlsx', sheet_name='substitution')
# steps = pd.read_excel('/Users/maggiewu/Desktop/CAAI/temp_substitution.xlsx', sheet_name='steps')
# print(substitutions.head())
# print(steps.head())

def get_substitution_steps_in(sub_id):
    """returns a list of Step_id"""
    lst = []
    for i, action in enumerate(steps['Action notes']):
        if (action != 'X') & (action is not None) & (action == substitutions.iloc[sub_id, :]['Steps in']):
            lst.append(steps.id[i])
    return lst

def get_substitution_steps_out(sub_id):
    """returns a list of Step_id"""
    lst = []
    for i, action in enumerate(steps['Action notes']):
        # if i != 'X' and i is not None:
        #     print(i)
        if (action != 'X') & (action is not None) & (action == substitutions.iloc[sub_id, :]['Steps out']):
            lst.append(steps.id[i])
    return lst

# print(get_substitution_steps_in(31))

def get_substitution_step_changes(sub_id):
    """returns a dict of delta values"""
    sub_changes = {}
    sub_changes['complexity'] = substitutions.iloc[sub_id, :]['Δcomplexity']
    sub_changes['time'] = 1 if substitutions.iloc[sub_id, :]['Δduration'] else 0 #will be modified to the exact time
    sub_changes['action'] = 1 if substitutions.iloc[sub_id, :]['Δactions'] else 0
    sub_changes['qualia'] = substitutions.iloc[sub_id, :]['Δqualia (cookies)']
    return sub_changes

# print(get_substitution_step_changes(0))

def get_step_values(step_id):
    """get step metadata. returns a dict of 'Required items', 'Duration', 'Complexity'"""
    step_values = {}

    step_values['required items'] = steps.loc[steps.id == step_id, 'Required items'].values[0]
    step_values['time'] = steps.loc[steps.id == step_id, 'Duration'].values[0]      #will be modified
    step_values['complexity'] = steps.loc[steps.id == step_id, 'Complexity'].values[0]

    return step_values
# print(get_step_values('Step_31'))

def sub_score(sub_id):
    in_items_num = len(substitutions.iloc[sub_id,:]['Items in'])
    out_items_num = len(substitutions.iloc[sub_id,:]['Items out'])
    num_of_items = in_items_num - out_items_num

    steps_in = get_substitution_steps_in(sub_id)
    steps_out = get_substitution_steps_out(sub_id)
    in_score, out_score = 0, 0
    for step in steps_in:
        in_score += get_step_values(step)['complexity']
    for step in steps_out:
        out_score += get_step_values(step)['complexity']

    change_in_complexity = get_substitution_step_changes(sub_id)['complexity'] + in_score - out_score
    time = 0
    efficacy = 0
    user_rating = 0

    score_ = num_of_items + change_in_complexity + time + efficacy + user_rating
    return score_

# notes: will have an 'delta item change' column

# print(sub_score(31))

def find_substitutions(recipe_name, desired_qualia, sub_ids=None):
    """return a list of substitution IDs"""
    recipe_subs = substitutions[substitutions['Scope'].str.contains(recipe_name)]
    recipe_subs = recipe_subs.dropna(subset = ['Δqualia (cookies)'])
    # qualia_subs = recipe_subs[recipe_subs[f'Δqualia ({recipe_name})'].str.contains(desired_qualia)]
    # #change 'Δqualia (cookies)' as 'Δqualia (Choc chip cookies)' and then use this line
    qualia_subs = recipe_subs[recipe_subs['Δqualia (cookies)'].str.contains(desired_qualia)]
    sub_ids = [x for x in qualia_subs.id]

    return sub_ids

# print(find_substitutions('Choc chip cookies', 'Nutty'))

def rank_substitutions(recipe_name, desired_qualia):
    sub_ids = find_substitutions(recipe_name, desired_qualia)
    sub_keys = sub_ids
    sub_values = list(map(sub_score, sub_ids))
    sub_dict = {sub_keys[i]: sub_values[i] for i in range(len(sub_keys))}

    sorted_dict = dict(sorted(sub_dict.items(), key = lambda item: item[1]))
    return list(sorted_dict.keys())

# print(rank_substitutions('Choc chip cookies', 'Nutty'))

